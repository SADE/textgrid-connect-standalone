xquery version "3.1";

module namespace tconnt="https://sade.textgrid.de/ns/connect/tests";

import module namespace connect="https://sade.textgrid.de/ns/connect" at "../modules/connect.xqm";
import module namespace test="http://exist-db.org/xquery/xqsuite" at "resource:org/exist/xquery/lib/xqsuite/xqsuite.xql";

declare namespace tgmd="http://textgrid.info/namespaces/metadata/core/2010";

declare
    %test:setUp
function tconnt:set-up()
as xs:string+ {
    let $relaxNG :=
        <grammar 
            xmlns="http://relaxng.org/ns/structure/1.0"
            xmlns:a="http://relaxng.org/ns/compatibility/annotations/1.0"
            ns="http://www.tei-c.org/ns/1.0">
            <start>
                <element name="TEI" xmlns="http://relaxng.org/ns/structure/1.0"><text/></element>
            </start>
        </grammar>
    return
        xmldb:store("/db/data/textgrid/data", "test.xml.rng", $relaxNG)
};

declare
    %test:tearDown
function tconnt:tear-down()
as item()+ {
    xmldb:remove("/db/data/textgrid/data", "test-txt.txt"),
    xmldb:remove("/db/data/textgrid/meta", "store-meta.xml"),
    xmldb:remove("/db/data/textgrid/data", "test-xml.xml"),
    xmldb:remove("/db/data/textgrid/data", "test-xml2.xml"),
    xmldb:remove("/db/data/textgrid/agg", "test-agg.xml"),
    xmldb:remove("/db/data/textgrid/rdf", "test-rdf.xml"),
    xmldb:remove("/db/data/textgrid/tile", "test-tile.xml"),
    xmldb:remove("/db/data/textgrid/data", "test-xsd.xsd.xml"),
    xmldb:remove("/db/data/textgrid/data", "test-rng.xml.rng"),
    xmldb:remove("/db/data/textgrid/data", "test.xml.rng"),
    xmldb:remove("/db/data/textgrid/data", "test-xml-validation-okay.xml"),
    xmldb:remove("/db/data/textgrid/data", "test-rng-publication-okay.xml"),
    file:delete("/exist/Predigtliteratur")
};

declare
    %test:args("admin", "somePassword") %test:assertTrue
function tconnt:is-user-authorized($user as xs:string,
    $password as xs:string)
as xs:boolean {
    connect:is-user-authorized($user, $password)
};


declare
    %test:args("https://textgridlab.org/1.0/tgcrud/rest", "1a2b3.4", "1234") %test:assertEquals("https://textgridlab.org/1.0/tgcrud/rest/1a2b3.4/data?sessionId=1234")
    %test:args("https://textgridlab.org/1.0/tgcrud/rest", "1a2b3.4", "") %test:assertEquals("https://textgridlab.org/1.0/tgcrud/rest/1a2b3.4/data?sessionId=")
function tconnt:make-request-url-for-data($tgcrud-url as xs:string,
    $public-uri as xs:string,
    $sid as xs:string)
as xs:string {
    connect:make-request-url-for-data($tgcrud-url, $public-uri, $sid)
};

declare
    %test:args("https://textgridlab.org/1.0/tgcrud/rest", "1a2b3.4", "1234") %test:assertEquals("https://textgridlab.org/1.0/tgcrud/rest/1a2b3.4/metadata?sessionId=1234")
    %test:args("https://textgridlab.org/1.0/tgcrud/rest", "1a2b3.4", "") %test:assertEquals("https://textgridlab.org/1.0/tgcrud/rest/1a2b3.4/metadata?sessionId=")
function tconnt:make-request-url-for-metadata($tgcrud-url as xs:string, 
    $public-uri as xs:string,
    $sid as xs:string)
as xs:string {
    connect:make-request-url-for-metadata($tgcrud-url, $public-uri, $sid)
};

declare
    %test:assertEquals("jfsn.xml")
function tconnt:make-target-uri-no-preservation()
as xs:string {
    let $metadata-file := local:get-metadata-sample()
    return
        connect:make-target-uri($metadata-file)
};

declare
    %test:assertEquals("jfsn.0.xml")
    %test:pending
function tconnt:make-target-uri-preservation()
as xs:string {
    let $metadata-file := local:get-metadata-sample()
    return
        connect:make-target-uri($metadata-file)
};

declare
    %test:assertEquals("https://textgridlab.org/1.0/tgsearch-public")
function tconnt:get-server-url-public()
as xs:string {
    let $metadata-file := local:get-metadata-sample()
    return
        connect:get-server-url($metadata-file)
};

declare
    %test:assertEquals("https://textgridlab.org/1.0/tgsearch")
function tconnt:get-server-url-non-public()
as xs:string {
    let $metadata-file := <MetadataContainerType xmlns="http://textgrid.info/namespaces/metadata/core/2010"/>
    return
        connect:get-server-url($metadata-file)
};


declare function local:get-metadata-sample()
as element(tgmd:MetadataContainerType) {
    <MetadataContainerType xmlns="http://textgrid.info/namespaces/metadata/core/2010"
        xmlns:tg="http://textgrid.info/relation-ns#"
        xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
        <object>
            <generic>
                <provided>
                    <title>Predigtliteratur</title>
                    <format>text/tg.aggregation+xml</format>
                </provided>
                <generated>
                    <created>2011-12-15T15:27:27.587+01:00</created>
                    <lastModified>2011-12-15T15:27:27.587+01:00</lastModified>
                    <issued>2011-12-15T15:27:27.587+01:00</issued>
                    <textgridUri extRef="">textgrid:jfsn.0</textgridUri>
                    <revision>0</revision>
                    <pid pidType="handle">hdl:11858/00-1734-0000-0001-CCA3-C</pid>
                    <extent>527</extent>
                    <dataContributor>tvitt@textgrid.de</dataContributor>
                    <project id="TGPR-372fe6dc-57f2-6cd4-01b5-2c4bbefcfd3c">Digitale Bibliothek</project>
                    <permissions>read</permissions>
                    <availability>public</availability>
                </generated>
            </generic>
            <item>
                <rightsHolder id="">TextGrid</rightsHolder>
            </item>
        </object>
    </MetadataContainerType>
};

declare
    %test:args("image/png") %test:assertTrue
    %test:args("image/jpeg") %test:assertTrue
    %test:args("text/xml") %test:assertFalse
function tconnt:is-image($format as xs:string)
as xs:boolean {
    connect:is-image($format)
};

declare
    %test:args("text/tg.aggregation+xml") %test:assertTrue
    %test:args("text/tg.collection+tg.aggregation+xml") %test:assertTrue
    %test:args("text/tg.edition+tg.aggregation+xml") %test:assertTrue
    %test:args("text/xml") %test:assertFalse
function tconnt:is-aggregation($format as xs:string)
as xs:boolean {
    connect:is-aggregation($format)
};

declare
    %test:args("text/tg.aggregation+xml") %test:assertEquals("agg")
    %test:args("text/tg.collection+tg.aggregation+xml") %test:assertEquals("agg")
    %test:args("text/tg.edition+tg.aggregation+xml") %test:assertEquals("agg")
    %test:args("text/xml+xslt") %test:assertEquals("data")
    %test:args("text/xsd+xml") %test:assertEquals("data")
    %test:args("text/tg.inputform+rdf+xml") %test:assertEquals("rdf")
    %test:args("text/linkeditorlinkedfile") %test:assertEquals("tile")
function tconnt:determine-target-collection($format as xs:string)
as xs:string {
    connect:determine-target-collection($format)
};


declare
(:    %test:args("text/tg.aggregation+xml") %test:assertTrue:)
(:    %test:args("text/tg.collection+tg.aggregation+xml") %test:assertTrue:)
(:    %test:args("text/tg.edition+tg.aggregation+xml") %test:assertTrue:)
    %test:args("text/xml+xslt") %test:assertTrue
    %test:args("text/tg.inputform+rdf+xml") %test:assertTrue
    %test:args("text/xml") %test:assertFalse
    %test:args("text/xsd+xml") %test:assertFalse
    %test:args("text/linkeditorlinkedfile") %test:assertTrue
    %test:args("text/plain") %test:assertFalse
function tconnt:is-no-further-processing-necessary($format as xs:string)
as xs:boolean {
    connect:is-no-further-processing-necessary($format)
};

declare
    %test:assertEquals("/db/data/textgrid/data/test-txt.txt")
function tconnt:process-plain-text()
as xs:string {
    let $data-file := "Some text."
    let $targetUri := "test-txt.xml"
    let $metadata-file := local:get-metadata-sample()
    return
        connect:process-plain-text($data-file, $targetUri, $metadata-file)
};

declare
    %test:assertTrue
function tconnt:is-lucene-config-true()
as xs:boolean {
    let $metadata :=
        <MetadataContainerType xmlns="http://textgrid.info/namespaces/metadata/core/2010">
            <object>
                <generic>
                    <provided>
                        <title>synonyms.txt</title>
                    </provided>
                </generic>
            </object>
        </MetadataContainerType>
    return
        connect:is-lucene-config($metadata)
};

declare
    %test:assertFalse
function tconnt:is-lucene-config-false()
as xs:boolean {
    let $metadata :=
        <MetadataContainerType xmlns="http://textgrid.info/namespaces/metadata/core/2010">
            <object>
                <generic>
                    <provided>
                        <title>charmap</title>
                    </provided>
                </generic>
            </object>
        </MetadataContainerType>
    return
        connect:is-lucene-config($metadata)
};

declare
    %test:assertEquals("Predigtliteratur")
function tconnt:get-title()
as xs:string {
    let $metadata-file := local:get-metadata-sample()
    return
        connect:get-title($metadata-file)
};

declare
    %test:args("text/xml") %test:assertTrue
function tconnt:store-metadata($format as xs:string)
as xs:boolean {
    let $intermediate-cleanup := 
        if (doc-available("/db/data/textgrid/meta/store-meta.xml")) then
            xmldb:remove("/db/data/textgrid/meta", "store-meta.xml")
        else
            ()
    let $targetUri := "store-meta.xml"
    let $meta := <MetadataContainerType xmlns="http://textgrid.info/namespaces/metadata/core/2010"/>
    let $store := connect:store-metadata($format, $targetUri, $meta)
    return
        doc-available("/db/data/textgrid/meta/store-meta.xml")
};

declare
    %test:assertTrue
function tconnt:process-lucene-config-files()
as xs:boolean {
    let $text := text{"Some text"}
    let $metadata-file := local:get-metadata-sample()
    let $process := connect:process-lucene-config-files($text, $metadata-file)
    return
        file:exists(system:get-exist-home() || util:system-property("file.separator") || "Predigtliteratur")
};

declare
    %test:args("text/xml", "test-xml.xml") %test:assertTrue
    %test:args("text/tg.aggregation+edition", "test-agg.xml") %test:assertTrue
    %test:args("text/tg.inputform+rdf+xml", "test-rdf.xml") %test:assertTrue
    %test:args("text/linkeditorlinkedfile", "test-tile.xml") %test:assertTrue
function tconnt:store($format as xs:string,
    $uri as xs:string)
as xs:boolean {
    let $data := <dummy/>
    let $path-to-stored := connect:store($data, $format, $uri)
    return
        doc-available($path-to-stored)
};

declare
    %test:args("text/xsd+xml", "test-xsd.xml") %test:assertTrue
function tconnt:process-xsd($format as xs:string,
    $targetUri as xs:string)
as xs:boolean {
    let $xsd :=
        <xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
        	<xsl:template match="@*|node()">
        		<xsl:copy>
        			<xsl:apply-templates select="@*|node()" />
        		</xsl:copy>
        	</xsl:template>
        </xsl:stylesheet>
    let $process := connect:process-xsd($xsd, $format, $targetUri)
    return
        doc-available("/db/data/textgrid/data/test-xsd.xsd.xml")
};

declare 
    %test:assertEquals("test.xml.rng")
function tconnt:make-xml-filename-rng()
as xs:string {
    let $targetUri := "test.xml"
    let $data := document { <grammar xmlns="http://relaxng.org/ns/structure/1.0"/> }
    return
        connect:make-xml-filename($data, $targetUri)
}; 

declare 
    %test:assertEquals("test.xml")
function tconnt:make-xml-filename-no-rng()
as xs:string {
    let $targetUri := "test.xml"
    let $data := document { <TEI xmlns="http://www.tei-c.org/ns/1.0"/> }
    return
        connect:make-xml-filename($data, $targetUri)
};

declare
    %test:assertTrue
function tconnt:store-xml()
as xs:boolean {
    let $targetUri := "test-xml2.xml"
    let $data := <TEI xmlns="http://www.tei-c.org/ns/1.0"/>
    let $store := connect:store-xml($data, $targetUri)
    return
        doc-available("/db/data/textgrid/data/test-xml2.xml")
};


declare
    %test:assertEquals("/db/data/textgrid/data/3r6zm.xml.rng")
function tconnt:get-relaxNG-path()
as xs:string {
    let $data :=
        document {
            processing-instruction xml-model {
                'href="textgrid:3r6zm" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"'
            },
            element {QName("http://www.tei-c.org/ns/1.0", "TEI")}{}
        }
    return
        connect:get-relaxNG-path($data)
};

declare
    %test:assertTrue
function tconnt:is-validatable-true()
as xs:boolean {
    let $relaxNGPath := xmldb:store("/db/data/textgrid/data", "test-rng.xml.rng", <rng/>)
    return
        connect:is-validatable($relaxNGPath)
};

declare
    %test:assertFalse
function tconnt:is-validatable-false()
as xs:boolean {
    let $relaxNGPath := "/db/data/textgrid/data/qwerty.xml.rng"
    return
        connect:is-validatable($relaxNGPath)
};

declare
    %test:assertTrue
function tconnt:is-xml-valid()
as xs:boolean {
    let $data :=
        document {
            processing-instruction xml-model {
                'href="textgrid:test" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"'
            },
            element {QName("http://www.tei-c.org/ns/1.0", "TEI")}{}
        }
    return
        connect:is-xml-valid($data)
};

declare
    %test:assertError
function tconnt:is-xml-valid-RNG-non-existent()
as xs:boolean {
    let $data :=
        document {
            processing-instruction xml-model {
                'href="textgrid:qwerty1234" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"'
            },
            element {QName("http://www.tei-c.org/ns/1.0", "TEI")}{}
        }
    return
        connect:is-xml-valid($data)
};


declare
    %test:assertFalse
function tconnt:is-xml-valid-invalid-xml()
as xs:boolean {
    let $data :=
        document {
            processing-instruction xml-model {
                'href="textgrid:test" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"'
            },
            element {QName("http://www.tei-c.org/ns/1.0", "teiHeader")}{}
        }
    return
        connect:is-xml-valid($data)
};

declare
    %test:assertEquals("/db/data/textgrid/data/test-xml-validation-okay.xml")
function tconnt:process-xml()
as xs:string {
    let $data :=
        document {
            processing-instruction xml-model {
                'href="textgrid:test" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"'
            },
            element {QName("http://www.tei-c.org/ns/1.0", "TEI")}{}
        }
    return
        connect:process-xml($data, local:get-metadata-sample(), "test-xml-validation-okay.xml", true())
};

declare
    %test:assertEquals("warning: The XML file ""Predigtliteratur"" with the URI test-xml-invalid.xml cannot be stored. Is the file valid and the associated schema available in the database? If not, please correct any mistakes in test-xml-invalid.xml and/or upload the associated schema first.")
function tconnt:process-xml-invalid-xml()
as xs:string {
    let $data :=
        document {
            processing-instruction xml-model {
                'href="textgrid:test" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"'
            },
            element {QName("http://www.tei-c.org/ns/1.0", "teiHeader")}{}
        }
    return
        connect:process-xml($data, local:get-metadata-sample(), "test-xml-invalid.xml", true())
};

declare
    %test:assertEquals("/db/data/textgrid/data/test-rng-publication-okay.xml.rng")
function tconnt:process-xml-rng-publication()
as xs:string {
    let $data :=
        document {
            <grammar xmlns="http://relaxng.org/ns/structure/1.0"/>
        }
    return
        connect:process-xml($data, local:get-metadata-sample(), "test-rng-publication-okay.xml", true())
};

declare
    %test:assertTrue
function tconnt:is-relaxNG-true()
as xs:boolean {
    let $data := <grammar xmlns="http://relaxng.org/ns/structure/1.0"/>
    return
        connect:is-relaxNG($data)
};

declare
    %test:assertTrue
function tconnt:is-relaxNG-true-document-node()
as xs:boolean {
    let $data := document {<grammar xmlns="http://relaxng.org/ns/structure/1.0"/>}
    return
        connect:is-relaxNG($data)
};

declare
    %test:assertFalse
function tconnt:is-relaxNG-false()
as xs:boolean {
    let $data := document {<grammar xmlns="http://someotherschema.org/ns/structure/1.0"/>}
    return
        connect:is-relaxNG($data)
};

declare
    %test:assertTrue
function tconnt:is-resource-available-true()
as xs:boolean {
    let $data-request-header :=
        <hc:response xmlns:hc="http://expath.org/ns/http-client" status="200" message="" spent-millis="300">
            <hc:header name="server" value="nginx"/>
            <hc:header name="date" value="Thu, 08 Oct 2020 09:20:33 GMT"/>
            <hc:header name="content-type" value="text/xml"/>
            <hc:header name="transfer-encoding" value="chunked"/>
            <hc:header name="connection" value="close"/>
            <hc:header name="last-modified" value="Sun, 05 Feb 2012 01:01:22 GMT"/>
            <hc:header name="vary" value="Accept-Encoding"/>
            <hc:header name="x-clacks-overhead" value="GNU Terry Pratchett"/>
            <hc:body media-type="text/xml"/>
        </hc:response>
    return
        connect:is-resource-available($data-request-header)
};

declare
    %test:assertFalse
function tconnt:is-resource-available-false()
as xs:boolean {
    let $data-request-header :=
        <hc:response xmlns:hc="http://expath.org/ns/http-client" status="404" message="" spent-millis="300">
            <hc:header name="server" value="nginx"/>
            <hc:header name="date" value="Thu, 08 Oct 2020 09:20:33 GMT"/>
            <hc:header name="content-type" value="text/xml"/>
            <hc:header name="transfer-encoding" value="chunked"/>
            <hc:header name="connection" value="close"/>
            <hc:header name="last-modified" value="Sun, 05 Feb 2012 01:01:22 GMT"/>
            <hc:header name="vary" value="Accept-Encoding"/>
            <hc:header name="x-clacks-overhead" value="GNU Terry Pratchett"/>
            <hc:body media-type="text/xml"/>
        </hc:response>
    return
        connect:is-resource-available($data-request-header)
};

declare
    %test:args("https://textgridlab.org/1.0/tgcrud/rest/", "")
    %test:assertXPath("count($result) = 3 and $result = 'textgrid:2xfn3' ")
function tconnt:get-image-uris($tgcrud-url as xs:string, 
    $sid as xs:string?)
as xs:string* {
    let $descendant-aggregated-uris :=
        (
            "textgrid:2xfn3",
            "textgrid:2xfn5",
            "textgrid:2xwh8",
            "textgrid:2xfn1",
            "textgrid:2xwh2"
        )
    return
        connect:get-image-uris($descendant-aggregated-uris, $tgcrud-url, $sid)
};

declare
    %test:assertXPath("count($result) = 2 and $result = 'textgrid:2xwh2' ")
function tconnt:get-non-image-uris()
as xs:string* {
    let $descendant-aggregated-uris :=
        (
            "textgrid:2xfn3",
            "textgrid:2xfn5",
            "textgrid:2xwh8",
            "textgrid:2xfn1",
            "textgrid:2xwh2"
        )
    let $image-uris :=
        (
            "textgrid:2xfn3",
            "textgrid:2xfn5",
            "textgrid:2xwh8"
        )
    return
        connect:get-non-image-uris($descendant-aggregated-uris, $image-uris)
};
