xquery version "3.1";
(:~
 : Script providing access to the test functions (XQSuite) for local unit test
 : execution.
 :)

import module namespace testtrigger="https://sade.textgrid.de/ns/connect/testtrigger" at "trigger-test.xq";

testtrigger:execute-tests()