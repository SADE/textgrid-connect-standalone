xquery version "3.1";

module namespace tclientt="https://sade.textgrid.de/ns/client/tests";

import module namespace client="https://sade.textgrid.de/ns/client" at "../modules/client.xqm";
import module namespace test="http://exist-db.org/xquery/xqsuite" at "resource:org/exist/xquery/lib/xqsuite/xqsuite.xql";

declare namespace sparql-results="http://www.w3.org/2005/sparql-results#";
declare namespace tgmd="http://textgrid.info/namespaces/metadata/core/2010";


declare
    %test:args("1a2b3.4", "https://textgridlab.org/1.0/tgcrud/rest", "1234") %test:assertEquals("https://textgridlab.org/1.0/tgcrud/rest/1a2b3.4/metadata?sessionId=1234")
    %test:args("1a2b3.4", "https://textgridlab.org/1.0/tgcrud/rest", "") %test:assertEquals("https://textgridlab.org/1.0/tgcrud/rest/1a2b3.4/metadata?sessionId=")
function tclientt:make-metadata-request-URL($id as xs:string,
    $tgcrud-url as xs:string,
    $sid as xs:string)
as xs:string {
    client:make-metadata-request-URL($id, $tgcrud-url, $sid)
};

declare
    %test:args("1a2b3.4", "https://textgridlab.org/1.0/tgcrud/rest", "1234") %test:assertEquals("https://textgridlab.org/1.0/tgcrud/rest/1a2b3.4/data?sessionId=1234")
    %test:args("1a2b3.4", "https://textgridlab.org/1.0/tgcrud/rest", "") %test:assertEquals("https://textgridlab.org/1.0/tgcrud/rest/1a2b3.4/data?sessionId=")
function tclientt:make-data-request-URL($id as xs:string,
    $tgcrud-url as xs:string,
    $sid as xs:string)
as xs:string {
    client:make-data-request-URL($id, $tgcrud-url, $sid)
};


declare
    %test:args("textgrid:jmzc", "https://textgridlab.org/1.0/tgcrud/rest", "") %test:assertXPath("$result//text() = 'Auf das schöne Geschlecht'")
    %test:pending %test:assumeInternetAccess("https://textgridlab.org/1.0/tgcrud/rest/")
function tclientt:get-metadata($id as xs:string,
    $tgcrud-url as xs:string,
    $sid as xs:string?)
as element(tgmd:MetadataContainerType) {
    client:get-metadata($id, $tgcrud-url, $sid)
    => local:fix-namespace()
};

declare
    %test:args("jmzc", "https://textgridlab.org/1.0/tgcrud/rest", "")
    %test:assertError("client02")
    %test:pending %test:assumeInternetAccess("https://textgridlab.org/1.0/tgcrud/rest/")
function tclientt:get-metadata-error($id as xs:string,
    $tgcrud-url as xs:string,
    $sid as xs:string?)
as node() {
    client:get-metadata($id, $tgcrud-url, $sid)
    => local:fix-namespace()
};


declare
    %test:args("https://textgridlab.org/1.0/tgcrud/rest/textgrid:jmzc/metadata?sessionId=", "textgrid:jmzc") %test:assertXPath("$result//text() = 'Auf das schöne Geschlecht'")
    %test:pending %test:assumeInternetAccess("https://textgridlab.org/1.0/tgcrud/rest/")
function tclientt:send-metadata-request($request-url as xs:string,
    $id as xs:string)
as node() {
    client:send-metadata-request($request-url, $id)
    => local:fix-namespace()
};


declare
    %test:args("https://textgridlab.org/1.0/tgcrud/rest/textgrid:qwerty/metadata?sessionId=", "textgrid:qwerty")
    %test:assertError("client02")
    %test:pending %test:assumeInternetAccess("https://textgridlab.org/1.0/tgcrud/rest/")
function tclientt:send-metadata-request-error($request-url as xs:string,
    $id as xs:string)
as node() {
    client:send-metadata-request($request-url, $id)
    => local:fix-namespace()
};

declare function local:fix-namespace($node as element())
as item() {
    let $namespace-uri := $node/namespace-uri()
    let $prefix :=
        switch ($namespace-uri)
            case "http://www.w3.org/2005/sparql-results#" return "sparql-results"
            case "http://textgrid.info/namespaces/metadata/core/2010" return "tgmd"
            default return ""
    return
           element {QName ($prefix,
                            local-name($node))}
           {$node/@*, $node/node()}
};

declare
    %test:args("textgrid:1234") %test:assertEquals("1234")
    %test:args("textgrid:with:more:colons:1234") %test:assertEquals("withmorecolons1234")
    %test:args("no_prefix") %test:assertEquals("no_prefix")
function tclientt:remove-prefix($tguri as xs:string)
as xs:string {
    client:remove-prefix($tguri)
};

declare
    %test:args("textgrid:vv6g", "https://textgridlab.org/1.0/tgcrud/rest/", "") %test:assertXPath("$result//*[local-name(.) = 'title'] = 'Biographie: Strachwitz, Moritz von'")
function tclientt:get-data($id as xs:string,
    $tgcrud-url as xs:string,
    $sid as xs:string?)
as item() {
    client:get-data($id, $tgcrud-url, $sid)//*[local-name(.) = "TEI"]
    => local:fix-namespace()
};

declare
    %test:args("textgrid:vv6g", "https://textgridlab.org/1.0/tgcrud/rest/", "") %test:assertXPath("$result/@status = '200' ")
function tclientt:get-data-request-header($id as xs:string,
    $tgcrud-url as xs:string,
    $sid as xs:string?)
as item() {
    client:get-data-request-header($id, $tgcrud-url, $sid)
};

declare
    %test:args("https://textgridlab.org/1.0/tgcrud/rest/textgrid:jkj1/data?sessionId=")
    %test:assertXPath("count($result) = 2")
function tclientt:make-request($request-url as xs:string) {
    client:make-request($request-url)
};

declare
    %test:args("https://textgridlab.org/1.0/tgcrud/rest/textgrid:qwerty1234/data?sessionId=")
    %test:assertError
function tclientt:make-request-error($request-url as xs:string) {
    client:make-request($request-url)
};

declare
    %test:args("textgrid:vqmz", "https://textgridlab.org/1.0/tgcrud/rest", "") %test:assertXPath("$result[1]/@status = '200'")
    %test:args("textgrid:12345asdf", "https://textgridlab.org/1.0/tgcrud/rest", "") %test:assertXPath("$result[1]/@status = '404'")
function tclientt:get-data-request-objects($id as xs:string,
    $tgcrud-url as xs:string,
    $sid as xs:string?)
as node()+ {
    client:get-data-request-objects($id, $tgcrud-url, $sid)
};
