xquery version "3.1";

(:~
 : Providing an API endpoint for triggering test execution.
 : This endpoint has been established instead of the text execution in post-install.xq
 : since at this point the RESTXQ API isn't fired up yet which causes the tests to throw errors.
 : 
 : @author Michelle Weidling
 : @version 0.1.0
 : @since 0.4.0
 :)

module namespace testtrigger="https://sade.textgrid.de/ns/connect/testtrigger";

import module namespace rest="http://exquery.org/ns/restxq";
import module namespace test="http://exist-db.org/xquery/xqsuite" at "resource:org/exist/xquery/lib/xqsuite/xqsuite.xql";

import module namespace tconnt="https://sade.textgrid.de/ns/connect/tests" at "connect-tests.xqm";
import module namespace tclientt="https://sade.textgrid.de/ns/client/tests" at "client-tests.xqm";

(:~
 : Triggers the tests for the Ahikar backend. Called by the CI.
 : 
 : @param $token A CI token
 : @return item() A log message to std out. In the Docker environment, this goes to exist.log.
 : @error The deploy token provided is incorrect
 :)
declare
  %rest:GET
  %rest:path("/trigger-tests")
  %rest:query-param("token", "{$token}")
function testtrigger:trigger($token)
as item()? {
  if( $token ne environment-variable("APP_DEPLOY_TOKEN" ))
    then error(QName("error://1", "deploy"), "Deploy token incorrect.")
  else
    let $ordered-results := testtrigger:execute-tests()
    
    let $fileSeparator := util:system-property("file.separator")
    let $system-path := system:get-exist-home() || $fileSeparator
    
    let $testWrap := <tests time="{current-dateTime()}">{ $ordered-results }</tests>
    
    let $filename := $system-path || "test-results.xml"
    let $file := file:serialize($testWrap, $filename, ())
    
    return
        util:log-system-out("Tests complete. See " || $filename)
};

declare function testtrigger:execute-tests()
as element()+ {
    let $tests := 
        (
            test:suite(util:list-functions("https://sade.textgrid.de/ns/connect/tests")),
            test:suite(util:list-functions("https://sade.textgrid.de/ns/client/tests"))
        )
        
    let $results := for $result in $tests return
        if ($result//@failures = 0
        and $result//@errors = 0) then
            <OK name="{$result//@package}" package="{$result//@package}"/>
        else
            <PROBLEM name="{$result//@package}"
                package="{$result//@package}"
                errors="{$result//@errors}"
                failures="{$result//@failures}">
                {$result//testcase[child::*[self::failure or self::error]]}
            </PROBLEM>
            
    for $result in $results
    order by $result/name() descending return
        $result
};
