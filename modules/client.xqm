xquery version "3.1";
(:~
 : The TextGrid Client offers interfaces to the main features of TextGrid,
 : when they are available via REST.
 : @version 1.1
 :)

module namespace client="https://sade.textgrid.de/ns/client";

declare namespace http="http://expath.org/ns/http-client";
declare namespace rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#";
declare namespace sparql-results="http://www.w3.org/2005/sparql-results#";
declare namespace tgmd="http://textgrid.info/namespaces/metadata/core/2010";
declare namespace tgs="http://www.textgrid.info/namespaces/middleware/tgsearch";

(:~
 : @param $id - the TextGrid ID of the object, e.g. textgrid:1234.5
 :)

declare function client:get-metadata($id as xs:string,
    $tgcrud-url as xs:string,
    $sid as xs:string?)
as node() {
    let $request-url := client:make-metadata-request-URL($id, $tgcrud-url, $sid)
    let $result := client:send-metadata-request($request-url, $id)
    return
        if( count($result) != 1 ) then
            <error> { $id } </error>
        else
            $result
};



declare function client:make-metadata-request-URL($id as xs:string,
    $tgcrud-url as xs:string,
    $sid as xs:string?)
as xs:string {
    $tgcrud-url || "/" || $id || "/metadata?sessionId=" || $sid
};


declare function client:send-metadata-request($request-url as xs:string,
    $id as xs:string)
as node()? {
    let $result := client:make-request($request-url)
    return
        if ($result//tgmd:MetadataContainerType) then
            $result//tgmd:MetadataContainerType
        else
            let $error := 
            (: error(QName("https://sade.textgrid.de/ns/client", "client02"), :)
             ("No metadata object could be fetched for " 
            || $id || " from " || $request-url || ".")
            return util:log("error", $error)
};


declare function client:get-data-request-objects($id as xs:string,
    $tgcrud-url as xs:string,
    $sid as xs:string?)
as item()+ {
    let $request-url := client:make-data-request-URL($id, $tgcrud-url, $sid)
    return
        client:make-request($request-url)
};


declare function client:get-data($id as xs:string,
    $tgcrud-url as xs:string,
    $sid as xs:string?)
as item() {
    client:get-data-request-objects($id, $tgcrud-url, $sid)[2]
};

declare function client:get-data-request-header($id as xs:string,
    $tgcrud-url as xs:string,
    $sid as xs:string?)
as item()+ {
    let $request-url := client:make-data-request-URL($id, $tgcrud-url, $sid)
    let $header :=
      <http:request method="head">
        <http:header name="Connection" value="close" />
      </http:request>
    return
        try {
            http:send-request($header, $request-url)
        } catch * {
            error(QName("https://sade.textgrid.de/ns/client", "client03"), "No header could be fetched from " || $request-url || ".")
        }
};


declare function client:make-data-request-URL($id as xs:string,
    $tgcrud-url as xs:string,
    $sid as xs:string?)
as xs:string {
    $tgcrud-url || "/" || $id || "/data?sessionId=" || $sid
};


declare function client:make-request($request-url as xs:string)
as item()+ {
    try {
        let $header :=
            <http:request method="get">
                <http:header name="Connection" value="close" />
            </http:request>
        return
            http:send-request($header, $request-url)
        } catch * {
            let $header :=
                <http:request method="get" override-media-type="text/plain">
                    <http:header name="Connection" value="close" />
                </http:request>
            let $response := http:send-request($header, $request-url)
            return
                ($response[1],
                <error time="{current-dateTime()}">
                            <code>{$err:code}</code>
                            <line>{$err:line-number}</line>
                            <column>{$err:column-number}</column>
                            <description>{$err:description}</description>
                            <value>{$err:value}</value>
                            <module>{$err:module}</module>
                            <additional>{$err:additional}</additional>
                            <document>{$response[2]}</document>
                </error>)
        }
};

(:~ Returns a list of the latest TextGrid items within a given aggregation.
 : @param tguri – the URI of any tg.aggregation
 : @param rest-url – url to the triple store
 : @param sid - the session ID
 : @return sequence of strings (textgrid URIs) or empty sequence
 :)
declare function client:get-latest-aggregated-uris($tguri as xs:string,
    $rest-url as xs:string,
    $sid as xs:string)
as xs:string* {
    let $result := client:get-rest-result($tguri, $rest-url, $sid)
    let $uris-recursive := client:get-revisioned-aggregated-uris($result, $rest-url, $sid)
    return ($tguri, $uris-recursive)
};


declare function client:get-rest-result($tguri as xs:string,
    $rest-url as xs:string,
    $sid as xs:string)
as element(tgs:response) {
    let $url := $rest-url || "/navigation/agg/" || $tguri || "?sid=" || $sid
    let $header :=
      <http:request method="get">
        <http:header
          name = "accept"
          value = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8"
        />
        <http:header
          name = "connection"
          value = "close"
        />
      </http:request>

    return http:send-request($header, $url)[2]/node()
};

declare function client:get-revisioned-aggregated-uris($result as element(tgs:response),
    $rest-url as xs:string,
    $sid as xs:string)
as item()* {
    let $aggregated := $result//tgs:result
    for $a in $aggregated return
        let $uri := $a/@textgridUri/string()
        let $revision := $a//tgmd:revision/text()
        return
            if (contains($a//tgmd:format, "aggregation")) then
                (
                    $uri,
                    client:get-latest-aggregated-uris($uri, $rest-url, $sid)
                )
            else
                $uri || "." || $revision
};

(:
 : Removes the `textgrid:` prefix from a given URI.
 :)
declare function client:remove-prefix($tguri as xs:string) as xs:string {
    if (contains($tguri, ":")) then
        tokenize($tguri, ":")[position() gt 1] => string-join()
    else
        $tguri
};

(:~
 : tg-search query function
 :)
declare %private function local:tgsearch-request(
    $tgsearch-url as xs:anyURI,
    $query as xs:string,
    $limit as xs:nonNegativeInteger,
    $start as xs:nonNegativeInteger,
    $filters as xs:string*,
    $sessionId as xs:string?)
as element(tgs:response) {
    let $params := (
        "q=" || encode-for-uri($query),
        "limit=" || $limit,
        "start=" || $start,
        "sid=" || $sessionId,
        $filters
    )
    
    let $get-params := string-join($params, "&amp;")
    let $request  := <hc:request method="get" href="{$tgsearch-url}/search?{$get-params}" />
    let $make-request := hc:send-request($request)
    let $responseHeader := $make-request[1]
    let $response := $make-request[2]
    
    return
        $response/*
};

(:
 : tg-search client: /search
 : @param $tgsearch-url: endpoint address
 : @param $query: query according to lucene syntax
 : @param $limit: number of results returned (per page)
 : @param $start: zero based offset to start listing hits
 : @param $filers: filter to apply to the query, incl. the `filter=` param name
 : @param $sessionId: a valid textgrid sessionId to query nonpublic resoures.
 :)
declare function client:tgsearch-search(
    $tgsearch-url as xs:anyURI,
    $query as xs:string,
    $limit as xs:nonNegativeInteger?,
    $start as xs:nonNegativeInteger?,
    $filters as xs:string*,
    $sessionId as xs:string?)
as element(tgs:result)*
{
    let $limit := if ( exists($limit)) then $limit else 50
    let $start := if ( exists($start)) then $start else 0
    let $query := if ( exists($query) and $query ne "") then $query else "*"

    let $init := local:tgsearch-request($tgsearch-url, $query, $limit, $start, $filters, $sessionId)
    let $hits := xs:nonNegativeInteger($init/@hits)
    let $turns := ($hits -1) idiv $limit +1 (: cutting edge :)

    let $results :=
        if ($turns eq 1) then $init
        else
            ($init, (: we do not repeat the first query, so we have to add :)
            for $i in 1 to $turns
            let $start := $i * $limit
            return
                local:tgsearch-request($tgsearch-url, $query, $limit, $start, $filters, $sessionId)
            )    
    return (
        $results//tgs:result
    )
};