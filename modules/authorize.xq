xquery version "3.1";
(:~
 : Checks if a user is authenticated in the database.
 :)

import module namespace connect="https://sade.textgrid.de/ns/connect" at "connect.xqm";

let $user as xs:string := request:get-parameter("user", ""),
    $password as xs:string := request:get-parameter("password", "")

return
    if(connect:is-user-authorized($user, $password)) then
        <div>
            <ok>User {$user} is logged in and authorized to publish to the database.</ok>
        </div>
    else
        error(QName("https://sade.textgrid.de/ns/error", "PUBLISH03"), "Incorrect username or password for user " || $user || ". Cannot publish to the database.")
