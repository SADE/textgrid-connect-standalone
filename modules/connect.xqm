xquery version "3.1";

(:~
 : This module provides the functions to transfer data from TextGrid to this
 : instance. It will be called via JavaScript XHR requests from index.html or
 : – programmatically – from other XQuery applications.
 :)

module namespace connect="https://sade.textgrid.de/ns/connect";

import module namespace config="http://sub.uni-goettingen.de/tg-connect/config" at "config.xqm";
import module namespace client="https://sade.textgrid.de/ns/client" at "client.xqm";

declare namespace tei="http://www.tei-c.org/ns/1.0";
declare namespace tgmd="http://textgrid.info/namespaces/metadata/core/2010";


(:~ The main function for the publisher.
 : @param $uri – the TextGrid URI of a single object
 : @param $sid – a valid SessionId able to get the objects
 : @param $user – a username for the db
 : @param $password – the password for the user in the db
 : @param $strictValidation – if true it will allow valid resources and rng only. rng must be present first!
 : @param $recursive – add all aggregated objects as well
 :)
declare function connect:publish($uri as xs:string,
    $sid as xs:string,
    $user as xs:string,
    $password as xs:string,
    $strictValidation as xs:boolean,
    $recursive as xs:boolean)
as item()+ {
    let $test-for-auth := 
        if (connect:is-user-authorized-externally()) then
            true()
        else
            connect:is-user-authorized($user, $password)
    return
        if (not($test-for-auth))
        then (error( 
                QName("https://sade.textgrid.de/ns/error", "PUBLISH02"),
                "error authenticating for " || $user || " on " || $config:data-root
                ))
        else
            let $tgcrud-url := config:get-value-from-configfile("textgrid.tgcrud")
            let $metadataContainer := client:get-metadata($uri, $tgcrud-url, $sid)
            let $tguri := $metadataContainer//tgmd:textgridUri/string()
            let $rdfstoreUrl := connect:get-server-url($metadataContainer)
            let $descendant-aggregated-uris := 
                if($recursive) then 
                    client:get-latest-aggregated-uris($tguri, $rdfstoreUrl, $sid)
                else
                    $tguri    
            let $image-map :=
                    connect:get-image-uris-with-tgsearch(
                        $descendant-aggregated-uris,
                        "filter=project.id:" || string($metadataContainer//tgmd:project/@id),
                        $sid
                    )
    
            let $store-images := connect:store-images($image-map)
            let $non-image-uris := connect:get-non-image-uris($descendant-aggregated-uris, map:keys($image-map))
            let $number-of-items-to-publish := count($non-image-uris)
            let $log := util:log-system-out("got a total of " || $number-of-items-to-publish || " non-image URIs to publish.")
            return
                (: handle the non-images :)
                for $public-uri at $pos in $non-image-uris return
                    let $log := util:log-system-out($pos || "/" || $number-of-items-to-publish)
                    let $data-request-objects := client:get-data-request-objects($public-uri, $tgcrud-url, $sid)
                    let $header := $data-request-objects[1]
                    let $data-file := $data-request-objects[2]
                    return
                        if (connect:is-resource-available($header)) then
                            connect:process-data($public-uri, $tgcrud-url, $sid, $tguri, $metadataContainer, $data-file, $strictValidation)
                        else
                            "Resource " || $public-uri || " not found."
};

(:~ The main function for the publisher.
 : Externally authenticated.
 : @param $uri – the TextGrid URI of a single object
 : @param $sid – a valid SessionId able to get the objects
 :)
declare function connect:publish($uri as xs:string,
    $sid as xs:string)
as item()+ {
    connect:publish($uri, $sid, "", "")
};

(:~ The main function for the publisher.
 : With strict validation.
 : @param $uri – the TextGrid URI of a single object
 : @param $sid – a valid SessionId able to get the objects
 : @param $user – a username for the db
 : @param $password – the password for the user in the db
 :)
declare function connect:publish($uri as xs:string,
    $sid as xs:string,
    $user as xs:string,
    $password as xs:string)
as item()+ {
    connect:publish($uri, $sid, $user, $password, true())
};

(:~ The main function for the publisher.
 : With strict validation, recursive.
 : @param $uri – the TextGrid URI of a single object
 : @param $sid – a valid SessionId able to get the objects
 : @param $user – a username for the db
 : @param $password – the password for the user in the db
 :)
declare function connect:publish($uri as xs:string,
    $sid as xs:string,
    $user as xs:string,
    $password as xs:string,
    $strictValidation as xs:boolean)
as item()+ {
    connect:publish($uri, $sid, $user, $password, true(), true())
};

(:
 : @deprecated
 :)
declare function connect:get-image-uris($descendant-aggregated-uris as xs:string+,
    $tgcrud-url as xs:string, 
    $sid as xs:string)
as xs:string* {
    let $presentUris := doc($config:data-root || "/images.xml")//image[substring-before(@uri, ".") = $descendant-aggregated-uris]/substring-before(@uri, ".")
    let $newUris := 
        for $uri in $descendant-aggregated-uris[not(. = $presentUris)]
        let $header := client:get-data-request-header($uri, $tgcrud-url, $sid)
        where starts-with($header//hc:header[@name = "content-type"]/@value, "image")
        return
            $uri
    return
        ($newUris, $presentUris[. ne ""])
};

declare function connect:get-image-uris-with-tgsearch(
    $descendant-aggregated-uris as xs:string+,
    $filters as xs:string+,
    $sid as xs:string)
as map(*) {
    let $tgsearch-url := config:get-value-from-configfile("textgrid.nonpublic") => xs:anyURI()
    let $hits :=
        client:tgsearch-search(
            $tgsearch-url,
            "format:""image/*""",
            100,
            0,
            $filters,
            $sid) (: returns element(tgs:result)* :)
    let $image-uris := $hits//tgmd:textgridUri/text()
    let $intersection := $descendant-aggregated-uris[. = $image-uris]
    let $presentUris := doc($config:data-root || "/images.xml")
                            //image[substring-before(@uri, ".") = $descendant-aggregated-uris]
                            /substring-before(@uri, ".")
    return
        map:merge(
            for $hit in $hits
            let $uri := $hit//tgmd:textgridUri/text()
            where $uri = $descendant-aggregated-uris
            return
                map:entry($uri, $hit)
        )
};

declare function connect:get-non-image-uris($descendant-aggregated-uris as xs:string+,
    $image-uris as xs:string*)
as xs:string+ {
    $descendant-aggregated-uris[not(. = $image-uris)]
};

declare function connect:is-resource-available($data-request-header as element())
as xs:boolean {
    $data-request-header/@status = "200"
};

declare function connect:process-data($public-uri as xs:string,
    $tgcrud-url as xs:string,
    $sid as xs:string,
    $tguri as xs:string,
    $metadataContainer as element(tgmd:MetadataContainerType),
    $data-file as item(),
    $strictValidation as xs:boolean)
as item()+ {
    let $meta := connect:get-metadata($public-uri, $tguri, $metadataContainer, $tgcrud-url, $sid)
    let $targetUri := connect:make-target-uri($meta)
    let $format := string($meta//tgmd:format)
    return
        (
            connect:process-according-to-format($format, $meta, $targetUri, $data-file, $strictValidation),
            connect:store-metadata($format, $targetUri, $meta)
        )
};

(: we should not download the metadata twice :)
declare function connect:get-metadata($public-uri as xs:string,
    $tguri as xs:string,
    $metadataContainer as element(tgmd:MetadataContainerType),
    $tgcrud-url as xs:string,
    $sid as xs:string)
as element(tgmd:MetadataContainerType) {
    if($public-uri = $tguri) then
        $metadataContainer
    else
        client:get-metadata($public-uri, $tgcrud-url, $sid)
};


declare function connect:store-metadata($format as xs:string,
    $targetUri as xs:string,
    $meta as element(tgmd:MetadataContainerType))
as xs:string? {
    xmldb:store($config:data-root || "/meta", $targetUri, $meta, "text/xml")
};


declare function connect:process-according-to-format($format as xs:string,
    $meta as element(tgmd:MetadataContainerType),
    $targetUri as xs:string,
    $data-file as item(),
    $strictValidation as xs:boolean)
as item()+ {
    if (connect:is-no-further-processing-necessary($format)) then
        connect:store($data-file, $format, $targetUri)
    
    else if ($format = "text/xml") then
        connect:process-xml($data-file, $meta, $targetUri, $strictValidation)
        
    
    else if ($format = "text/xsd+xml") then
        connect:process-xsd($data-file, $format, $targetUri)
    
    else if ($format = "text/plain") then
        connect:process-plain-text($data-file, $targetUri, $meta)
    
    else
        "The publisher does not know how to handle " || $format || "."
};


declare function connect:is-no-further-processing-necessary($format as xs:string)
as xs:boolean {
    if (connect:is-aggregation($format)
    or $format = (
        "text/xml+xslt",
        "text/tg.inputform+rdf+xml",
        "text/linkeditorlinkedfile"
        )
    ) then
        true()
    else
        false()
};


declare function connect:process-xsd($data-file as item(),
    $format as xs:string,
    $targetUri as xs:string)
as xs:string {
    let $targetUri := replace($targetUri, "xml", "xsd.xml")
    return
        connect:store($data-file, $format, $targetUri)
};


declare function connect:store-xml($data as item(),
    $targetUri as xs:string)
as xs:string? {
    let $name := connect:make-xml-filename($data, $targetUri)
    return
        xmldb:store($config:data-root||"/data", $name, $data, "text/xml")
};


declare function connect:make-xml-filename($data as node(),
    $targetUri as xs:string)
as xs:string {
    if (connect:is-relaxNG($data)) then
        $targetUri || ".rng"
    else
        $targetUri
};

declare function connect:is-relaxNG($data as node())
as xs:boolean {
    $data/namespace-uri() = "http://relaxng.org/ns/structure/1.0"
    or $data/*/namespace-uri() = "http://relaxng.org/ns/structure/1.0"
};


declare function connect:process-xml($data as item(),
    $meta as element(tgmd:MetadataContainerType),
    $targetUri as xs:string,
    $strictValidation as xs:boolean)
as xs:string {
    if (connect:is-relaxNG($data)
        or connect:is-xml-valid($data)
        or not($strictValidation)) then
        connect:store-xml($data, $targetUri)
    else
        let $title := $meta//tgmd:title/string()
        return
           "warning: The XML file """ || $title || """ with the URI " || $targetUri || 
           " cannot be stored. Is the file valid and the associated schema available "
       || "in the database? If not, please correct any mistakes in " || $targetUri || 
       " and/or upload the associated schema first."
};


declare function connect:is-xml-valid($data as item())
as xs:boolean {
    let $relaxNGPath :=  connect:get-relaxNG-path($data)
    return
        if(connect:is-validatable($relaxNGPath)) then
             validation:jing($data, doc($relaxNGPath))
        else
            false()
};

declare function connect:is-validatable($relaxNGPath as xs:string)
as xs:boolean {
    doc-available($relaxNGPath)
};


declare function connect:get-relaxNG-path($data as item())
as xs:string {
    let $relaxNG-uri := $data/processing-instruction()
                        [contains(string(.), "http://relaxng.org/ns/structure/1.0")]
                        /substring-after(substring-before(., '" type'), "textgrid:")
    return
        $config:data-root || "/data/" || $relaxNG-uri || ".xml.rng"
};


declare function connect:process-plain-text($data-file as item(),
    $targetUri as xs:string,
    $metadata-file as element(tgmd:MetadataContainerType))
as xs:string {
    let $name := replace($targetUri, "xml", "txt")
    return
        if(connect:is-lucene-config($metadata-file)) then
            connect:process-lucene-config-files($data-file, $metadata-file)
        else
            xmldb:store-as-binary($config:data-root || "/data", $name, $data-file)
};


declare function connect:is-lucene-config($metadata-file as element(tgmd:MetadataContainerType))
as xs:boolean {
    connect:get-title($metadata-file) = ("synonyms.txt", "charmap.txt")
};



declare function connect:process-lucene-config-files($text as item(),
    $metadata-file as element(tgmd:MetadataContainerType))
as xs:string {
    let $title := connect:get-title($metadata-file)
    let $base64 := xs:base64Binary(util:base64-encode($text))
    let $path := system:get-exist-home() || util:system-property("file.separator")
    let $store-on-disk :=
        try     { file:serialize-binary($base64, $path || $title) }
        catch * { error(
                    QName("https://sade.textgrid.de/ns/error", "PUBLISH07"),
                    "storing Lucene configuration failed with " || $err:code || ": " || $err:description)
                }
    let $reindex := xmldb:reindex( $config:data-root || "/data" )
    return
        $path || $title
};


declare function connect:get-title($metadata-file as element(tgmd:MetadataContainerType))
as xs:string {
    $metadata-file//tgmd:title
};


(: we do not store images, but reference the published ones in the file images.xml :)
declare function connect:store-images($image-map as map(*))
{
    if (count(map:keys($image-map)) gt 0) then
        (
            if(doc-available( $config:data-root || "/images.xml" )) then
                connect:update-image-xml($image-map)
            else
                connect:create-and-update-image-xml($image-map),
            "Images: some metadata stored."
        )
    else
        ()
};


declare function connect:create-and-update-image-xml($image-map as map(*))
{
    xmldb:store($config:data-root, "images.xml", <images/>),
    connect:update-image-xml($image-map)
};


declare function connect:update-image-xml($image-map as map(*))
{
    let $doc := doc($config:data-root || "/images.xml")
    let $uris := $doc//image/string(@uri)
    let $image-entries :=
        for $uri in map:keys($image-map)
        where not($uri = $uris)
        let $metadata := map:get($image-map, $uri)
        return
            <image
                uri="{string($metadata//tgmd:textgridUri)}"
                title="{string($metadata//tgmd:title)}"
                format="{string($metadata//tgmd:format)}"
            />
    return
        if ($image-entries) then
            update insert $image-entries
            into $doc/images
        else
            ()
};


declare function connect:is-user-authorized($user as xs:string,
    $password as xs:string)
as xs:boolean {
    if(sm:id()//sm:group/string() = 'dba') then
        true()
    else
    xmldb:login($config:data-root, $user, $password)
};

declare function connect:is-user-authorized-externally()
as xs:boolean {
    if(sm:id()//sm:group/string() = 'dba') then
        true()
    else
        false()
};

declare function connect:make-request-url-for-data($tgcrud-url as xs:string,
    $public-uri as xs:string,
    $sid as xs:string?)
as xs:string {
    $tgcrud-url || "/" || $public-uri || "/data?sessionId=" || $sid
};


declare function connect:make-request-url-for-metadata($tgcrud-url as xs:string,
    $public-uri as xs:string,
    $sid as xs:string?)
as xs:string {
    $tgcrud-url || "/" || $public-uri || "/metadata?sessionId=" || $sid
};



declare function connect:make-target-uri($metadata-file as element(tgmd:MetadataContainerType)) {
    let $preserve-revisions as xs:boolean := config:get-value-from-configfile("textgrid.preserveRevisions") => xs:boolean()
    return
        client:remove-prefix($metadata-file//tgmd:textgridUri/text())
        !  (if($preserve-revisions)
            then string(.)
            else substring-before(., '.'))
        || ".xml"
};


declare function connect:get-server-url($metadata-file as element(tgmd:MetadataContainerType))
as xs:string {
    if ($metadata-file//tgmd:generated/tgmd:availability = "public") then
        config:get-value-from-configfile("textgrid.public")
    else
        config:get-value-from-configfile("textgrid.nonpublic")
};


declare function connect:is-image($format as xs:string)
as xs:boolean {
    starts-with($format, "image")
};


declare function connect:is-aggregation($format as xs:string)
as xs:boolean {
    contains($format, "tg.aggregation")
};


declare function connect:determine-target-collection($format as xs:string)
as xs:string {
    if (connect:is-aggregation($format)) then
        "agg"
    else if ($format = "text/xml+xslt"
    or $format = "text/xsd+xml") then
        "data"
    else if ($format = "text/tg.inputform+rdf+xml") then
        "rdf"
    else if ($format = "text/linkeditorlinkedfile") then
        "tile"
    else
        "data"
};


declare function connect:store($data as item(),
    $format as xs:string,
    $uri as xs:string)
as xs:string {
    let $subcollection := connect:determine-target-collection($format)
    let $target-collection := $config:data-root || '/' || $subcollection
    return
        xmldb:store($target-collection, $uri, $data, "text/xml")
};
