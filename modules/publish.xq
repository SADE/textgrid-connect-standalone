xquery version "3.1";
(:~
 : Provides the endpoint for SADE Publish process
 :)

import module namespace connect="https://sade.textgrid.de/ns/connect" at "connect.xqm";

let $uri as xs:string := request:get-parameter("uri", ""),
    $sid as xs:string := request:get-parameter("sid", ""),
    $user as xs:string := request:get-parameter("user", ""),
    $password as xs:string := request:get-parameter("password", ""),
    $strictMode as xs:boolean := request:get-parameter("strictMode", "") => xs:boolean(), (: string 'true' or 'false' :)
    $recursive as xs:boolean := request:get-parameter("recursive", "") => xs:boolean() (: string 'true' or 'false' :)

return
    if( $uri = "" ) then
        error(QName("https://sade.textgrid.de/ns/error", "PUBLISH01"), "no URI provided")
    else
        <div>{
            for $i in connect:publish($uri, $sid, $user, $password, $strictMode, $recursive) return
                if (contains($i, "warning")) then
                    <warning>{$i}</warning>
                else
                    <ok>{ $uri } » { $i }</ok>
        }</div>
