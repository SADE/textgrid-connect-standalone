# TextGrid Connect Standalone

[![semantic-release: angular](https://img.shields.io/badge/semantic--release-angular-e10079?logo=semantic-release)](https://github.com/semantic-release/semantic-release)

A simple application for transferring data from the TextGridLab to eXist-db.

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
## Table of Contents

- [TextGrid Connect Standalone](#textgrid-connect-standalone)
  - [Table of Contents](#table-of-contents)
  - [What it does](#what-it-does)
  - [What it doesn't do](#what-it-doesnt-do)
  - [Getting Started](#getting-started)
    - [Prerequisites](#prerequisites)
    - [Local development with Docker](#local-development-with-docker)
    - [Local development without Docker](#local-development-without-docker)
    - [Installing](#installing)
      - [Build the xar](#build-the-xar)
      - [Get it from DARIAH-DE's eXist repository](#get-it-from-dariah-des-exist-repository)
    - [Using the module within TextGrid](#using-the-module-within-textgrid)
  - [Running the tests](#running-the-tests)
  - [Versioning](#versioning)
  - [Authors](#authors)
  - [License](#license)
  - [Acknowledgments](#acknowledgments)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## What it does

* provides an HTML page for publishing in TextGridLab
* gets data from TextGrid's public and non-public REST interface
* stores this data to one of the collections `/db/data/textgrid/(data|agg|rdf|tile|meta)` if the data has been validated.
**Caution: Unvalidated data will not be stored in the database by default!** It is possilbe to disable the strict validation by unchecking the corresponding option (spell check icon) in the GUI. Strict validation is switched on by default.
* stores some metadata of published images instead of saving the whole image

## What it doesn't do

* pushing data to TextGrid
* creating data on the TextGrid server

**Limitation: Recursive publish process can deal with objects from a single project. Check where a `project.id` is in use.**

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.
See deployment for notes on how to deploy the project on a live system.

### Prerequisites

* ant
* docker *(for local development)*
* docker-compose 1.27.0 or higher *(for local development)*
* firefox *(for local development)*

### Local development with Docker

An eXist-db instance containing TextGrid Connect Standalone for local development can be started
with `build-local.sh`.
This sets up the Docker environment and opens eXist-db in your Firefox.

### Local development without Docker

Another option to start an eXist-db instance containing the application is to run

```bash
ant test
```

in the cloned directory.

Afterwards eXist can be started with `./test/bin/startup.sh`.
The database will then be available at <http://localhost:8080/>.

---

### Installing

#### Build the xar

The *.xar package can be build by executing

```bash
ant
```

in the repository's directory.
It will be stored in the `build` sub-directory and can be installed to any eXist-db instance via the package manager.

#### Get it from DARIAH-DE's eXist repository

You can also download the latest version of the app from DARIAH-DE's exist repository at<https://ci.de.dariah.eu/exist-repo/packages.html?package-id=tg-connect-standalone>.

It can be installed to any eXist-db instance via the package manager.

---

### Using the module within TextGrid

In order to use TextGrid Connect Standalone within the TextGridLab you have to have to SADE Publisher installed.

To achive this, start your TextGridLab and select `Help > Eclipse Market Place` in the top bar.
Search for "SADE Publish Tool" and install it.

Now you can configure it to show TextGrid Connect's HTML page by selecting `Window > Preferences > Sade Publisher` in the top bar.
Enter

```bash
${IP}/exist/apps/textgrid-connect/index.html
```

in the input field "URL to SADE publish.html" with `${IP}` being the address of your database.
Set the SADE user to `admin`, enter your database's admin password and save your changes.
The HTML interface of TextGrid Connect Standalone in now available when you try to publish to SADE (e.g. via context menu on items in the navigator).

## Running the tests

You can execute the test in the application by executing `tests/runner.xq`.

There is also a RESTXQ endpoint which automatically runs when `cURL`ed at

```bash
${IP}/exist/restxq/trigger-tests
```

The test result will be available as a file named `test-results.xml` in the project's home directory.

**Caution:** If you started the database locally with `ant test`, the resulting file will be stored in the `test` directory.
The test results are currently not available when using the Docker set up.

## Versioning

We use [SemVer](http://semver.org/) for versioning.
For the versions available, see the [tags on this repository](https://gitlab.gwdg.de/SADE/textgrid-connect-standalone/-/tags).

## Authors

* **Michelle Weidling** - *Setting up the standalone version* - [mrodzis](https://gitlab.gwdg.de/mrodzis)
* **Mathias Göbel** - *Nothing but placing his name here* [mgoebel](https://gitlab.gwdg.de/mgoebel)

See also the list of [contributors](https://gitlab.gwdg.de/SADE/textgrid-connect-standalone/-/graphs/develop) who participated in this project.

## License

This project is licensed under APGL 3.0 - see the [LICENSE](LICENSE) and [NOTICE](NOTICE) files for details

## Acknowledgments

Thanks a lot to [Mathias Göbel](https://gitlab.gwdg.de/mgoebel) and [Ubbo Veentjer](https://gitlab.gwdg.de/uveentj) who have written the original TextGrid connect version which is used within the [SADE framework](https://gitlab.gwdg.de/SADE/SADE/).

Kudos also to [Stefan Hynek](https://gitlab.gwdg.de/hynek) who updated the original version to eXist-db 5.2.0.
