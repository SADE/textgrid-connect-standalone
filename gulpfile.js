////////////////////////////////
// Setup
////////////////////////////////

// Gulp and package
const { src, dest, parallel, series } = require('gulp')

// Plugins
const concat = require('gulp-concat')
const plumber = require('gulp-plumber')
const rename = require('gulp-rename')
const uglify = require('gulp-uglify-es').default

// Relative paths function
function pathsConfig() {
  const vendorsRoot = 'node_modules'
  const templatesRoot = 'resources'

  return {
    vendorsJs: [
      `${vendorsRoot}/jquery/dist/jquery.js`,
    ],
    projectJs: [
        `${templatesRoot}/js/publish-gui.js`,
    ],
    js: `${templatesRoot}/js`,
  }
}

var paths = pathsConfig()

////////////////////////////////
// Tasks
////////////////////////////////

// Javascript minification:
function jscripts() {
    return src([`${paths.vendorsJs}`, `${paths.projectJs}`])
      .pipe(concat('project.js'))
      .pipe(dest(paths.js))
      .pipe(plumber()) // Checks for errors
      .pipe(uglify()) // Minifies the js
      .pipe(rename({ suffix: '.min' }))
      .pipe(dest(paths.js))
  }




// Generate all assets: prepared for parallel procession, if other tasks are added
const generateAssets = parallel(
  jscripts
)

exports.default = series(generateAssets)
exports["generate-assets"] = generateAssets

