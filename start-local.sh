#!/bin/bash

docker-compose up --detach

port=$(docker-compose ps | egrep -o "0\.0\.0\.0:[0-9]{5}\->8080" | egrep -o "0\.0\.0\.0:[0-9]{5}")

sleep 15

firefox $port
