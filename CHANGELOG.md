# [1.11.0](https://gitlab.gwdg.de/SADE/textgrid-connect-standalone/compare/v1.10.0...v1.11.0) (2024-05-14)


### Features

* use tgsearch response for storing image metadata ([0512056](https://gitlab.gwdg.de/SADE/textgrid-connect-standalone/commit/0512056df3024d150e3cddbb10736c2b520a4586))

# [1.10.0](https://gitlab.gwdg.de/SADE/textgrid-connect-standalone/compare/v1.9.0...v1.10.0) (2024-04-05)


### Features

* use tgsearch response for storing image metadata ([ce1815a](https://gitlab.gwdg.de/SADE/textgrid-connect-standalone/commit/ce1815a8d9bc67bc875c9bea290211f26f0efa75))

# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [2.1.1] - 2024-04-05

### Changed

- image metadata processing performance improved by using tg-search response already present

## [2.1.0] - 2024-03-27

### Added

- tgsearch client for the `/search` endpoint

### Changed

* using tgsearch for image-lookup ([a734b1d](https://gitlab.gwdg.de/SADE/textgrid-connect-standalone/commit/a734b1d0714fa4c2fa3270bfc8945611442d6307))

## [2.0.0] - 2023-12-11

### Changed

- all SPARQL requests have been replaced by REST requests to `tgsearch`

## [1.8.4] - 2023-12-06

### Changed

- exist-DB version increased to 6.0.1 in Docker build

### Fixed

- erroneous check for revision in `client.xq`. Formerly, a revision of `.0` led to an URI in the form of `textgrid:asdfg` which made a more performant handling of the images problematic. with this fix, URIs always have the form of `textgrid:{$URI}.{$REVISION}`
- only new image URI are stored to `image.xml`

## [1.8.3] - 2023-02-09

### Fixed

- icon for recursive mode

## [1.8.2](https://gitlab.gwdg.de/SADE/textgrid-connect-standalone/compare/v1.8.1...v1.8.2) (2023-12-11)

### Bug Fixes

* check ([ec6c1f3](https://gitlab.gwdg.de/SADE/textgrid-connect-standalone/commit/ec6c1f3100a4b32b32fd0bf5be1f22cfde911d16))
* icon for recursive mode ([f613ae3](https://gitlab.gwdg.de/SADE/textgrid-connect-standalone/commit/f613ae392003885cfd95e21e31450ecadb4eab50))
* only update when new image ([0abdfae](https://gitlab.gwdg.de/SADE/textgrid-connect-standalone/commit/0abdfae5a8367840c89bdd16253039bd0df6c24f))
* port binding ([8aa835c](https://gitlab.gwdg.de/SADE/textgrid-connect-standalone/commit/8aa835c677f780d76d4ff23698328f4490a3549b))
* syntax ([deff027](https://gitlab.gwdg.de/SADE/textgrid-connect-standalone/commit/deff027e67e79231b78c80946adb40453b6f35d1))


### Fixed

- syntax error in `publish.xq`

## [1.8.1] - 2022-04-05

### Fixed

- less strict metadata loading, as removed URIs are still in triple store

## [1.8.0] - 2021-09-10

### Added

- recursion can be disabled now
- image data will not be appended when already present

## [1.7.2] - 2021-07-20

### Fixed

- performance of HEAD request in `client.xqm`

## [1.7.1] - 2021-07-14

### Fixed

- the “less strict mode” for importing non-valid data now supports even non-well-formed xml by
storing the downloaded data as plain text in an error document with all available error messages.
Before the import breaks for non-well-formed data. This will leave the default “strict mode”
untouched as the imported document will not be valid.

## [1.7.0] - 2021-07-06

### Added

- “strict mode”: adds a switch for strict validation (default). The default value (`true`) will
publish data only when they are valid against a provided rng schema (must be present first!).
if set to `false` (= less strict mode) the process will check for valid xml only.

## [1.6.0] - 2021-07-05

### Added

- compatibility to eXist 5.3.0 (now with an dependency mapping in package metadata file expath-pkg.xml)

### Changes

- XQuery script files are renamed from “xql” to “xq”
- eXist binary file for build target “test” moved to GitHub

## [1.5.0] - 2021-07-05

### Changed

- the test output only provides detailed information on the results in case something has gone wrong.
- Docker images have been changed to (mostly) custom images.
these improve the performance of the pipelines and mitigate the dependency on external images.

### Fixed

- the path for retrieving eXist-db has been updated according to upstream changes.

## [1.4.0] - 2021-02-15

### Changed

- Images are not fully loaded during the publish process, but only their hc:response header is requested.
This dramatically reduces the process running time and fixes the issue that validation errors do not seem to be thrown in certain circumstances.

## [1.3.0] - 2021-01-20

### Changed

- User directly receive feedback in the status panel if they have successfully logged in and are able to publish data to the database.

## [1.2.0] - 2021-01-18

### Added

- This CHANGELOG to keep track of the development.

### Changed

- If an XML file is not valid or its schema file is missing, the warning output now also provides the file's title as shown in the TextGrid navigator.
This simplifies finding the file in focus.
