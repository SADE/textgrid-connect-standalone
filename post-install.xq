xquery version "3.1";

import module namespace config="http://sub.uni-goettingen.de/tg-connect/config" at "modules/config.xqm";

(: setting up the target collections fot the transmitted data if necessary :)
if(xmldb:collection-available($config:data-root || "/data/textgrid/")) then
    ()
else
    (
        xmldb:create-collection("/db", "data"),
        xmldb:create-collection("/db/data", "textgrid"),
        
        for $collection in ("data", "meta", "agg", "tile", "rdf") return
            xmldb:create-collection($config:data-root, $collection)
    ),

(: set owner and mode for testtrigger module :)
(let $path := "/db/apps/textgrid-connect/tests/trigger-test.xq"
return (sm:chown($path, "admin"), sm:chmod($path, "rwsrwxr-x")))