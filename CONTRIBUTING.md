# Contributing

We welcome any contributions to this repository!

The following is a set of guidelines for contributing to TextGrid Connect Standalone. These are mostly guidelines, not rules. Use your best judgment, and feel free to propose changes to this document in a merge request.

## Issue Tracker

Issues are created and assigned in the [issue tracker](https://gitlab.gwdg.de/SADE/textgrid-connect-standalone/-/issues) by anyone who discovers something worthy of improvement.

When you create a new issue, select the respective template ("feature" or "bug") and fill it in.
Make sure you add an appropriate label to the ticket and set a **weight** according to this key:

* **1**: the change this issue introduces is a *nice-to-have*
* **2**: this change `SHOULD` be implemented but it is *not pressing*
* **3**: this change `MUST` be implemented *in the near future* because it severely impedes further development

As soon as you start working on a assigned issue, switch its label to `Doing` so it is clear to others that this issue is in progress.
This also causes the issue to be moved into the right list of the repository's [board](https://gitlab.gwdg.de/SADE/textgrid-connect-standalone/-/boards).

## Merge Requests (MR)

Merge requests should be peer reviewed before merging them into `develop`.
A well-tried workflow is:

1. A developer decides to work on a feature.
She uses the current development branch as a base for her work.
She commits her changes to a separate feature branch that she has created by using [GitLab flow](https://docs.gitlab.com/ee/topics/gitlab_flow.html).
After some time she finishes the feature and wants it to be part of the development branch.
2. Before marking her merge request as ready, the developer merges the current state of `develop` in her feature branch.
This minimizes merge conflicts.
3. The developer assigns everybody she sees fit to properly review her code to it.
4. The assignee(s) review(s) the changes according to style, variable naming, understandability, documentation provided, functionality, etc.
If everything is to his or her liking, he or she approves the MR.
Any other assignees are free to review the code as well.
**Note:** MRs without docs and tests should not be accepted.
5. After the MR has been (dis)approved, the assignee removes his- or herself from the list of assignees.
6. The developer resolves any discussions that might have come up during the review.
7. The developer merges her changes into the development branch.

If a merge conflict occurs the person who has proposed the MR is responsible for solving all conflicts.

## General Guidelines

Be nice, constructive, and professional.
